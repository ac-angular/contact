import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-email-sender',
  templateUrl: './email-sender.component.html',
  styleUrls: ['./email-sender.component.css']
})
export class EmailSenderComponent implements OnInit {
  title = 'email-sender';
  contactForm = this.fb.group({
    data: this.fb.group({
      name: this.fb.control(''),
      email: this.fb.control(''),
      subject: this.fb.control('')
    }),
    message: this.fb.control('')
  });

  constructor(private fb: FormBuilder, private dataService: DataService) { }
  loading = false;
  buttonText = "Submit";


  onSubmit() {
    this.loading = true;
    this.buttonText = "Submiting...";
    let user = {
      name: this.contactForm.value.data.name,
      email: this.contactForm.value.data.email,
      subject: this.contactForm.value.data.subject,
      message: this.contactForm.value.message,
    }
    this.dataService.post("http://localhost:3000/sendemail", user).subscribe(
      data => {
        let res:any = data; 
        console.log(
          `👏 > ${user.name} is successfully register and email has been sent and the message id is ${res.messageId}`
        );
      },
      err => {
        console.log(err);
        this.loading = false;
        this.buttonText = "Submit";
      },() => {
        this.loading = false;
        this.buttonText = "Submit";
      }
    );
  }

  ngOnInit() {}
}
