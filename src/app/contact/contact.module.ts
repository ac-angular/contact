import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailSenderComponent } from './email-sender/email-sender.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MapComponent } from './map/map.component';
import { SharedModule } from '../shared/shared.module';
import { ContactInfoComponent } from './contact-info/contact-info.component';
import { SocialButtonsComponent } from './social-buttons/social-buttons.component';

@NgModule({
  declarations: [
    EmailSenderComponent,
    MapComponent,
    ContactInfoComponent,
    SocialButtonsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  exports:[     // public
    EmailSenderComponent,
    MapComponent,
    ContactInfoComponent,
    SocialButtonsComponent
  ]
})
export class ContactModule { }
