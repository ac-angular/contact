import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public static DATA = './../../assets/data.json';


  constructor(private httpClient: HttpClient) { }

  public getData() {
    return this.httpClient.get(DataService.DATA, {responseType: 'json'});
  }

  post(url, data) {
    return this.httpClient.post(url, data);
  }
}
