import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SanitizerPipe } from './pipes/sanitizer.pipe';

@NgModule({
  declarations: [SanitizerPipe],
  imports: [
    CommonModule
  ],
  exports: [
    SanitizerPipe
  ]
})
export class SharedModule { }
