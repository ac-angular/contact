import { SanitizerPipe } from './sanitizer.pipe';
import { DomSanitizer } from '@angular/platform-browser';
import { inject } from '@angular/core/testing';

describe('SanitizerPipe', () => {
  it('create an instance', inject([DomSanitizer], (domSanitizer: DomSanitizer) => {
    const pipe = new SanitizerPipe(domSanitizer);
    expect(pipe).toBeTruthy();
  }));
});
